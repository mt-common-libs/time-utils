package com.appspot.magtech.timeutils

import com.appspot.magtech.timeutils.lexer.DKA
import com.appspot.magtech.timeutils.lexer.DKAFactory
import com.appspot.magtech.timeutils.lexer.Token

internal enum class TimeTokenType {
    PRIME_MINUS, MINUS, NUMBER, DOT, D, H, M, S
}

internal class TimeUtilsParseException: Exception()

internal class CustomDKAFactory: DKAFactory {

    override fun createDKA(tokens: ArrayList<Token>): CustomDKA {
        val dka = CustomDKA(tokens)
        dka.generateJumpTable()
        return dka
    }
}

internal class CustomDKA(tokens: ArrayList<Token>): DKA() {

    private val buffer = StringBuilder("")

    private val pushMinus = { _: String, _: Char -> tokens += Token(TimeTokenType.MINUS, "-") }
    private val pushPrimeMinus = { _: String, _: Char -> tokens += Token(TimeTokenType.PRIME_MINUS, "!-") }
    private val pushToBuffer = { _: String, sym: Char -> buffer.append(sym); Unit }
    private val pushLetter = { letter: TimeTokenType ->
        { _: String, _: Char ->
            tokens += Token(TimeTokenType.NUMBER, buffer.toString())
            buffer.clear()
            tokens += Token(letter, letter.toString())
        }
    }
    private val pushDot = { _: String, _: Char -> tokens += Token(TimeTokenType.DOT, ".")}

    override val states = mapOf(
        "A" to emptyState,
        "B" to pushPrimeMinus,
        "C" to emptyState,
        "D" to pushMinus,
        "E" to emptyState,
        "F" to pushToBuffer,
        "G" to pushLetter(TimeTokenType.D),
        "H" to pushMinus,
        "I" to pushToBuffer,
        "K" to pushLetter(TimeTokenType.H),
        "L" to pushLetter(TimeTokenType.M),
        "M" to pushMinus,
        "N" to { prev: String, sym: Char ->
            if (prev != "N") {
                tokens += Token(TimeTokenType.NUMBER, buffer.toString())
                buffer.clear()
                pushDot(prev, sym)
            } else {
                pushToBuffer(prev, sym)
            }
        },
        "O" to pushLetter(TimeTokenType.S),
        "P" to pushToBuffer,
        "R" to pushMinus,
        "S" to pushToBuffer
    )

    override val startState = "A"

    override val endStates = arrayListOf("G", "K", "L", "O")

    override val jumps = mapOf(
        "-" to { sym: Char -> sym == '-' },
        "P" to { sym: Char -> sym == 'P' },
        "T" to { sym: Char -> sym == 'T' },
        "D" to { sym: Char -> sym == 'D' },
        "H" to { sym: Char -> sym == 'H' },
        "M" to { sym: Char -> sym == 'M' },
        "S" to { sym: Char -> sym == 'S' },
        "." to { sym: Char -> sym == '.' },
        "number" to { sym: Char -> sym in '0'..'9' }
    )

    override val jumpList = arrayListOf(
        "A" - "-" - "B",
        "A" - "P" - "C",
        "B" - "P" - "C",
        "C" - "-" - "D",
        "C" - "T" - "E",
        "C" - "number" - "F",
        "D" - "number" - "F",
        "E" - "-" - "H",
        "E" - "number" - "I",
        "F" - "D" - "G",
        "F" - "number" - "F",
        "G" - "T" - "E",
        "H" - "number" - "I",
        "I" - "H" - "K",
        "I" - "M" - "L",
        "I" - "S" - "O",
        "I" - "." - "N",
        "I" - "number" - "I",
        "K" - "-" - "M",
        "K" - "number" - "P",
        "L" - "-" - "R",
        "L" - "number" - "S",
        "M" - "number" - "P",
        "N" - "S" - "O",
        "N" - "number" - "N",
        "P" - "S" - "O",
        "P" - "." - "N",
        "P" - "number" - "P",
        "P" - "M" - "L",
        "R" - "number" - "S",
        "S" - "S" - "O",
        "S" - "." - "N",
        "S" - "number" - "S"
    )
}