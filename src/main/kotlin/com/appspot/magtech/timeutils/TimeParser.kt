package com.appspot.magtech.timeutils

import com.appspot.magtech.timeutils.lexer.Token
import java.lang.StringBuilder

internal class TimeParser {

    fun parse(tokens: List<Token>): Map<String, Double> {
        val map = mutableMapOf<String, Double>()
        with (tokens.iterator()) {
            var token = next()
            val isNegative = tokens.first().type == TimeTokenType.PRIME_MINUS
            if (isNegative) {
                token = next()
            }
            while (hasNext()) {
                val number = StringBuilder("")
                while (!token.isLetter()) {
                    number.append(token.value)
                    token = next()
                }
                var doubleValue = number.toString().toDouble()
                if (isNegative) {
                    doubleValue *= -1
                }
                map += token.value to doubleValue
                if (hasNext()) {
                    token = next()
                }
            }
        }
        return map
    }

    private fun Token.isLetter() =
        type in arrayOf(
            TimeTokenType.D,
            TimeTokenType.H,
            TimeTokenType.M,
            TimeTokenType.S
        )
}