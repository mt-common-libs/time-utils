package com.appspot.magtech.timeutils.lexer

import com.appspot.magtech.timeutils.TimeUtilsParseException

internal class Lexer(private val DKAFactory: DKAFactory) {

    fun analyze(str: String): List<Token> {
        val tokens = arrayListOf<Token>()
        val DKA = DKAFactory.createDKA(tokens)
        var currentState = DKA.startState

        for (sym in str) {
            var flagTrans = false
            for ((i, state) in DKA.jumpTable.getValue(currentState).withIndex()) {
                if (state != null && DKA.jumpValues[i](sym)) {
                    DKA.states.getValue(state)(currentState, sym)
                    currentState = state
                    // println(state)
                    flagTrans = true
                    break
                }
            }
            if (!flagTrans) {
                throw TimeUtilsParseException()
            }
        }
        if (currentState !in DKA.endStates) {
            throw TimeUtilsParseException()
        }
        return tokens
    }
}