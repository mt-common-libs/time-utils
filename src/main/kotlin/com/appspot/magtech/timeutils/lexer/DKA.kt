package com.appspot.magtech.timeutils.lexer

internal data class Token(val type: Enum<*>, val value: String)

internal interface DKAFactory {

    fun createDKA(tokens: ArrayList<Token>): DKA
}

internal typealias State = ((prevState: String, sym: Char) -> Unit)
internal typealias Jump = (sym: Char) -> Boolean

internal abstract class DKA {

    inner class StateWithJump(private val state: String, private val jump: String) {

        operator fun minus(st: String): Triple<String, String, String> {
            return Triple(state, jump, st)
        }
    }

    operator fun String.minus(jump: String): StateWithJump {
        return StateWithJump(this, jump)
    }

    val emptyState = { _: String, _: Char -> Unit }

    abstract val jumps: Map<String, Jump>
    abstract val states: Map<String, State>
    abstract val startState: String
    abstract val endStates: ArrayList<String>

    abstract val jumpList: ArrayList<Triple<String, String, String>>

    var jumpTable: MutableMap<String, Array<String?>> = mutableMapOf()
    var jumpValues = arrayOf<Jump>()

    fun generateJumpTable() {
        jumpValues = jumps.values.toTypedArray()
        for (state in states.keys) {
            jumpTable.put(state, arrayOfNulls(jumps.size))
        }
        for (record in jumpList) {
            jumpTable.getValue(record.first)[jumps.keys.indexOf(record.second)] = record.third
        }
    }
}