package com.appspot.magtech.timeutils

import com.appspot.magtech.timeutils.lexer.Lexer

private val timeLexer = Lexer(CustomDKAFactory())
private val timeParser = TimeParser()

fun parseDurationToSeconds(text: CharSequence): Long {
    val multiples = mapOf<String, Long>(
        "S" to 1,
        "M" to 1 * 60,
        "H" to 1 * 60 * 60,
        "D" to 1 * 60 * 60 * 24
    )
    return parseDuration(text, multiples)
}

fun parseDurationToMillis(text: CharSequence): Long {
    val multiples = mapOf<String, Long>(
        "S" to 1000,
        "M" to 1000 * 60,
        "H" to 1000 * 60 * 60,
        "D" to 1000 * 60 * 60 * 24
    )
    return parseDuration(text, multiples)
}

private fun parseDuration(text: CharSequence, multiples: Map<String, Long>): Long {
    val tokens = timeLexer.analyze(text.toString())
    val parseResult = timeParser.parse(tokens)
    var result = 0L
    parseResult.forEach { type: String, value: Double ->
        result += (value * multiples.getValue(type)).toLong()
    }
    return result
}