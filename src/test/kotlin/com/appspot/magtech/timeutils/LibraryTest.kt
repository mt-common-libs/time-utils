package com.appspot.magtech.timeutils

import com.appspot.magtech.timeutils.lexer.Lexer
import java.time.Duration
import java.time.format.DateTimeParseException
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class LibraryTest {

    private val rightCases = arrayListOf (
        "P20D",
        "P20DT10H8M10.345S",
        "P20DT0.45S",
        "PT10.S",
        "PT5.5S",
        "PT10H",
        "PT7M",
        "-P-20DT4H-8M-10S",
        "P5DT-8H7M",
        "PT-5.0S",
        "PT5.6455S"
    )

    private val wrongCases = arrayListOf (
        "PT",
        "P20D8H",
        "P5D7M",
        "P20D8M",
        "P5DT7M8H",
        "PT.5S"
    )

    @Test
    fun testSuccessMillisCases() {
        rightCases.forEach {
            assertEquals(Duration.parse(it).toMillis(), parseDurationToMillis(it))
        }
    }

    @Test
    fun testSuccessSecondsCases() {
        rightCases.forEach {
            assertEquals(Duration.parse(it).toSeconds(), parseDurationToSeconds(it))
        }
    }

    @Test
    fun testWrongCases() {
        wrongCases.forEach {
            assertFailsWith<TimeUtilsParseException> {
                Lexer(CustomDKAFactory()).analyze(it)
            }
            assertFailsWith<DateTimeParseException> {
                Duration.parse(it)
            }
        }
    }
}
