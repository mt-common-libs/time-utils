plugins {
    // Apply the Kotlin JVM plugin to add support for Kotlin on the JVM.
    application
    id("org.jetbrains.kotlin.jvm").version("1.3.31")
}

group = "com.appspot.magtech"
version = "1.0"

repositories {
    mavenCentral()
    mavenLocal()
    jcenter()
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    testImplementation("org.jetbrains.kotlin:kotlin-test")
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit")
}

val includedLibs = arrayListOf<String>()

application {
    tasks.register<Jar>("plugin") {
        dependsOn("compileKotlin")
        from("build/classes/kotlin/main")
        configurations["compileClasspath"].forEach { file: File ->
            if (file.name in includedLibs) {
                from(zipTree(file.absoluteFile))
            }
        }
    }
}

tasks.register<Exec>("publish") {
        dependsOn("plugin")
        commandLine("mvn", "install:install-file",
                "-Dfile=build/libs/time-utils-$version.jar",
                "-DgroupId=com.appspot.magtech",
                "-DartifactId=time-utils",
                "-Dversion=$version",
                "-Dpackaging=jar")
}

